package customer.experience;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import selenium.web.driver.commons.SeleniumBasePage;

public class LoginPage extends SeleniumBasePage {
    @FindBy(xpath = "/html/body/jhi-main/div[2]/div/jhi-login/div/div/div/h1")
    private WebElement signInPage;

    @FindBy(xpath = "//*[@id=\"username\"]")
    private WebElement userName;

    @FindBy(xpath = "//*[@id=\"password\"]")
    private WebElement password;

    @FindBy(xpath = "/html/body/jhi-main/div[2]/div/jhi-login/div/div/div/form/button")
    private WebElement signIn;

    public boolean isDisplayed () {
        maximizeWindow();
        getSignInPageHeader();
        return true;
    }

    public String getSignInPageHeader () {
        return signInPage.getText();
    }

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void inputUserName (String userNameInput) {
        inputText(userName, userNameInput);
    }

    public void inputPassword (String passwordInput) {
        inputText(password, passwordInput);
    }

    public CustomerExperiencePage clickSignIn () {
        signIn.click();
        return new CustomerExperiencePage(getWebDriver());
    }
}
