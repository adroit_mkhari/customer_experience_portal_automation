package files;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.Units;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;

public class ExcelWriter {
    private String filePath;
    private String sheetName;
    private int sheetIndex;
    // private XSSFWorkbook workbook;
    private SXSSFWorkbook workbook;
    private CellStyle style;
    private Font font;

    public ExcelWriter() {
    }

    public ExcelWriter(String filePath) {
        this.filePath = filePath;
    }

    public ExcelWriter(String filePath, String sheetName) {
        this.filePath = filePath;
        this.sheetName = sheetName;
    }

    public ExcelWriter(String filePath, int sheetIndex) {
        this.filePath = filePath;
        this.sheetIndex = sheetIndex;
    }

    public ExcelWriter(String filePath, String sheetName, SXSSFWorkbook workbook) {
        this.filePath = filePath;
        this.sheetName = sheetName;
        this.workbook = workbook;
    }

    public ExcelWriter(String filePath, int sheetIndex, SXSSFWorkbook workbook) {
        this.filePath = filePath;
        this.sheetIndex = sheetIndex;
        this.workbook = workbook;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public int getSheetIndex() {
        return sheetIndex;
    }

    public void setSheetIndex(int sheetIndex) {
        this.sheetIndex = sheetIndex;
    }

    public SXSSFWorkbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(SXSSFWorkbook workbook) {
        this.workbook = workbook;
    }

    public void createWorkbook() {
        workbook = new SXSSFWorkbook();
    }

    public SXSSFSheet addSheet() {
        if (workbook != null) {
            if (workbook.getSheet(sheetName) == null) {
                return workbook.createSheet(sheetName);
            } else {
                return workbook.getSheet(sheetName);
            }
        }
        return null;
    }

    public SXSSFSheet addSheet(String sheetName) {
        if (workbook != null) {
            if (workbook.getSheet(sheetName) == null) {
                return workbook.createSheet(sheetName);
            } else {
                return workbook.getSheet(sheetName);
            }
        }
        return null;
    }

    private void fontAndStyle() {
        if (style == null) {
            style = workbook.createCellStyle();
        }

        if (font == null) {
            font = workbook.createFont();
        }
    }

    public void writeToSheet(int rowNumber, int columnNumber, Object value) {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet() : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(rowNumber) == null ? sheet.createRow(rowNumber) : sheet.getRow(rowNumber);
        SXSSFCell cell = row.createCell(columnNumber) == null ? row.createCell(columnNumber) : row.getCell(columnNumber);
        cell.setCellValue((String) value);

        fontAndStyle();

        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setFontHeightInPoints((short) 11);
        font.setBold(false);
        style.setFont(font);

        cell.setCellStyle(style);
    }

    public void writeToSheet(String sheetName, int rowNumber, int columnNumber, Object value) {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet(sheetName) : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(rowNumber) == null ? sheet.createRow(rowNumber) : sheet.getRow(rowNumber);
        SXSSFCell cell = row.createCell(columnNumber) == null ? row.createCell(columnNumber) : row.getCell(columnNumber);
        cell.setCellValue((String) value);

        fontAndStyle();

        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setFontHeightInPoints((short) 11);
        font.setBold(false);
        style.setFont(font);

        cell.setCellStyle(style);
    }

    public void writeErrorToSheet(int rowNumber, int columnNumber, Object value) {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet() : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(rowNumber) == null ? sheet.createRow(rowNumber) : sheet.getRow(rowNumber);
        SXSSFCell cell = row.createCell(columnNumber) == null ? row.createCell(columnNumber) : row.getCell(columnNumber);
        cell.setCellValue((String) value);

        fontAndStyle();

        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setFontHeightInPoints((short) 11);
        font.setBold(false);
        font.setColor((short) 2);
        style.setFont(font);

        cell.setCellStyle(style);
    }

    public void writeErrorToSheet(String sheetName, int rowNumber, int columnNumber, Object value) {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet(sheetName) : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(rowNumber) == null ? sheet.createRow(rowNumber) : sheet.getRow(rowNumber);
        SXSSFCell cell = row.createCell(columnNumber) == null ? row.createCell(columnNumber) : row.getCell(columnNumber);
        cell.setCellValue((String) value);

        fontAndStyle();

        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setFontHeightInPoints((short) 11);
        font.setBold(false);
        font.setColor((short) 2);
        style.setFont(font);

        cell.setCellStyle(style);
    }

    public void writeWarningToSheet(int rowNumber, int columnNumber, Object value) {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet() : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(rowNumber) == null ? sheet.createRow(rowNumber) : sheet.getRow(rowNumber);
        SXSSFCell cell = row.createCell(columnNumber) == null ? row.createCell(columnNumber) : row.getCell(columnNumber);
        cell.setCellValue((String) value);

        fontAndStyle();

        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setFontHeightInPoints((short) 11);
        font.setBold(false);
        font.setColor((short) 4);
        style.setFont(font);

        cell.setCellStyle(style);
    }

    public void writeWarningToSheet(String sheetName, int rowNumber, int columnNumber, Object value) {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet(sheetName) : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(rowNumber) == null ? sheet.createRow(rowNumber) : sheet.getRow(rowNumber);
        SXSSFCell cell = row.createCell(columnNumber) == null ? row.createCell(columnNumber) : row.getCell(columnNumber);
        cell.setCellValue((String) value);

        fontAndStyle();

        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setFontHeightInPoints((short) 11);
        font.setBold(false);
        font.setColor((short) 4);
        style.setFont(font);

        cell.setCellStyle(style);
    }

    public void writeSuccessToSheet(int rowNumber, int columnNumber, Object value) {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet() : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(rowNumber) == null ? sheet.createRow(rowNumber) : sheet.getRow(rowNumber);
        SXSSFCell cell = row.createCell(columnNumber) == null ? row.createCell(columnNumber) : row.getCell(columnNumber);
        cell.setCellValue((String) value);

        fontAndStyle();

        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setFontHeightInPoints((short) 11);
        font.setBold(false);
        font.setColor((short) 3);
        style.setFont(font);

        cell.setCellStyle(style);
    }

    public void writeSuccessToSheet(String sheetName, int rowNumber, int columnNumber, Object value) {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet(sheetName) : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(rowNumber) == null ? sheet.createRow(rowNumber) : sheet.getRow(rowNumber);
        SXSSFCell cell = row.createCell(columnNumber) == null ? row.createCell(columnNumber) : row.getCell(columnNumber);
        cell.setCellValue((String) value);

        fontAndStyle();

        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setFontHeightInPoints((short) 11);
        font.setBold(false);
        font.setColor((short) 3);
        style.setFont(font);

        cell.setCellStyle(style);
    }

    public void writeHeardersToSheet(int rowNumber, int columnNumber, Object value) {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet() : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(rowNumber) == null ? sheet.createRow(rowNumber) : sheet.getRow(rowNumber);
        SXSSFCell cell = row.createCell(columnNumber) == null ? row.createCell(columnNumber) : row.getCell(columnNumber);
        cell.setCellValue((String) value);

        CellStyle style = workbook.createCellStyle();
        style.setBorderTop(BorderStyle.DOUBLE);
        style.setBorderBottom(BorderStyle.DOUBLE);
        style.setBorderLeft(BorderStyle.DOUBLE);
        style.setBorderRight(BorderStyle.DOUBLE);

        Font font = workbook.createFont();
        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        style.setFont(font);

        cell.setCellStyle(style);

        sheet.trackColumnForAutoSizing(columnNumber);
        sheet.autoSizeColumn((short) columnNumber);
    }

    public void writeHeardersToSheet(String sheetName, int rowNumber, int columnNumber, Object value) {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet(sheetName) : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(rowNumber) == null ? sheet.createRow(rowNumber) : sheet.getRow(rowNumber);
        SXSSFCell cell = row.createCell(columnNumber) == null ? row.createCell(columnNumber) : row.getCell(columnNumber);
        cell.setCellValue((String) value);

        CellStyle style = workbook.createCellStyle();
        style.setBorderTop(BorderStyle.DOUBLE);
        style.setBorderBottom(BorderStyle.DOUBLE);
        style.setBorderLeft(BorderStyle.DOUBLE);
        style.setBorderRight(BorderStyle.DOUBLE);

        Font font = workbook.createFont();
        font.setFontName(HSSFFont.FONT_ARIAL);
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        style.setFont(font);

        cell.setCellStyle(style);
    }

    public void writePictureToSheet(String sheetName, int rowNumber, int columnNumber, byte[] imageBytes) {
        try {
            SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet(sheetName) : workbook.getSheet(sheetName);
            int pictureIdx = workbook.addPicture(imageBytes, Workbook.PICTURE_TYPE_PNG);

            CreationHelper helper = workbook.getCreationHelper();
            Drawing drawing = sheet.createDrawingPatriarch();
            ClientAnchor anchor = helper.createClientAnchor();
            anchor.setCol1(columnNumber);
            anchor.setRow1(rowNumber);
            Picture pict = drawing.createPicture(anchor, pictureIdx);
            pict.resize();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  void adjustColumnSizes() {
        SXSSFSheet sheet = workbook.getSheet(sheetName) == null ? addSheet() : workbook.getSheet(sheetName);
        SXSSFRow row = sheet.getRow(0) == null ? sheet.createRow(0) : sheet.getRow(0);
        for (int i = 0; i < row.getLastCellNum(); i++) {
            boolean columnTrackedForAutoSizing = sheet.isColumnTrackedForAutoSizing(i);
            if (columnTrackedForAutoSizing) {
                sheet.autoSizeColumn((short) i);
            }
        }
    }

    public void createFile() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(filePath);
        workbook.write(fileOutputStream);
        // workbook.close();
        // fileOutputStream.close();
    }
}
