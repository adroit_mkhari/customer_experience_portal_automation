package customer.experience;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import selenium.web.driver.commons.SeleniumBasePage;

public class CustomerExperiencePage extends SeleniumBasePage {
    @FindBy(xpath = "//*[@id=\"page-heading\"]/span")
    private WebElement customerExperiencePageHeader;

    @FindBy(xpath = "//*[@id=\"page-heading\"]/div/button[1]/span/input")
    private WebElement search;

    @FindBy(xpath = "//*[@id=\"page-heading\"]/div/button[2]/span")
    private WebElement refreshList;

    @FindBy(xpath = "//*[@id=\"jh-create-entity\"]/span")
    private WebElement createNewCustomer;

    @FindBy(xpath = "//*[@id=\"entities\"]/table/tbody/tr/td[2]")
    private WebElement firstName;

    @FindBy(xpath = "//*[@id=\"entities\"]/table/tbody/tr/td[3]")
    private WebElement lastName;

    @FindBy(xpath = "//*[@id=\"entities\"]/table/tbody/tr/td[4]")
    private WebElement cellphoneNumber;

    @FindBy(xpath = "//*[@id=\"entities\"]/table/tbody/tr/td[5]")
    private WebElement idNumber;

    @FindBy(xpath = "//*[@id=\"entities\"]/table/tbody/tr/td[6]")
    private WebElement status;

    @FindBy(xpath = "//*[@id=\"entities\"]/table/tbody/tr/td[7]/div/button[1]/span")
    private WebElement view;

    @FindBy(xpath = "//*[@id=\"entities\"]/table/tbody/tr/td[7]/div/button[2]/span")
    private WebElement edit;

    @FindBy(xpath = "//*[@id=\"entities\"]/table/tbody/tr/td[7]/div/button[3]/span")
    private WebElement delete;

    public CustomerExperiencePage(WebDriver webDriver) {
        super(webDriver);
    }

    public boolean isDisplayed () {
        getCustomerExperiencePageHeader();
        return true;
    }

    public String getCustomerExperiencePageHeader() {
        return customerExperiencePageHeader.getText();
    }

    public void inputSearchText(String searchInput) throws InterruptedException {
        clickRefreshList();
        inputText(search, searchInput);
    }

    public void clickRefreshList() throws InterruptedException {
        refreshList.click();
        Thread.sleep(1000);
    }

    public CreateOrEditCustomer clickCreateNewCustomer() {
        createNewCustomer.click();
        return new CreateOrEditCustomer(getWebDriver());
    }

    public String getFirstName() {
        return firstName.getText();
    }

    public String getLastName() {
        return lastName.getText();
    }

    public String getCellphoneNumber() {
        return cellphoneNumber.getText();
    }

    public String getIdNumber() {
        return idNumber.getText();
    }

    public String getStatus() {
        return status.getText();
    }

    public void clickView() {
        view.click();
    }

    public void clickEdit() throws InterruptedException {
        edit.click();
        Thread.sleep(2000);
    }

    public void clickDelete() {
        delete.click();
    }
}
