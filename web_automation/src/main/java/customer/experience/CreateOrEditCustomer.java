package customer.experience;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import selenium.web.driver.commons.SeleniumBasePage;

public class CreateOrEditCustomer extends SeleniumBasePage {
    @FindBy(xpath = "//*[@id=\"jhi-customer-heading\"]")
    private WebElement pageHeader;

    @FindBy(xpath = "//*[@id=\"field_firstName\"]")
    private WebElement firstName;

    @FindBy(xpath = "//*[@id=\"field_lastName\"]")
    private WebElement lastName;

    @FindBy(xpath = "//*[@id=\"field_cellPhoneNumber\"]")
    private WebElement cellPhoneNumber;

    @FindBy(xpath = "//*[@id=\"field_idNumber\"]")
    private WebElement idNumber;

    @FindBy(xpath = "//*[@id=\"save-entity\"]/span")
    private WebElement save;

    public CreateOrEditCustomer(WebDriver webDriver) {
        super(webDriver);
    }

    public boolean isDisplayed() {
        getPageHeader();
        return true;
    }

    public String getPageHeader() {
        return pageHeader.getText();
    }

    public void inputFirstName(String firstNameInput) {
        clearAndInputText(firstName, firstNameInput);
    }

    public void inputLastName(String lastNameInput) {
        clearAndInputText(lastName, lastNameInput);
    }

    public void inputCellPhoneNumber(String cellPhoneNumberInput) {
        clearAndInputText(cellPhoneNumber, cellPhoneNumberInput);
    }

    public void inputIdNumber(String idNumberInput) {
        clearAndInputText(idNumber, idNumberInput);
    }

    public void clickSave() {
        save.click();
    }

    public void createOrEditCustomer(String firstNameInput,
                                     String lastNameInput,
                                     String cellPhoneNumberInput,
                                     String idNumberInput) throws InterruptedException {
        if (!firstNameInput.isEmpty()) {
            inputFirstName(firstNameInput);
            Thread.sleep(500);
        }

        if (!lastNameInput.isEmpty()) {
            inputLastName(lastNameInput);
            Thread.sleep(500);
        }

        if (!cellPhoneNumberInput.isEmpty()) {
            inputCellPhoneNumber(cellPhoneNumberInput);
            Thread.sleep(500);
        }

        if (!idNumberInput.isEmpty()) {
            inputIdNumber(idNumberInput);
            Thread.sleep(500);
        }

        clickSave();
        Thread.sleep(2000);
    }
}
