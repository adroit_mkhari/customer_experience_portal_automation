package co.za.mamamoney.customer;

import co.za.mamamoney.customer.model.customer.Customer;
import com.fasterxml.jackson.core.JsonProcessingException;
import files.RecordReader;
import files.TestCase;
import io.cucumber.datatable.DataTable;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomerBase {
    protected Properties properties;
    protected static DataTable dataTable;
    protected static Object[] headers;
    protected static TestCase testCase;
    protected RecordReader recordReader;
    protected Customer customer;

    public Customer getCustomer(List<String> dataEntry) throws Exception {
        recordReader = new RecordReader(headers, "", dataEntry);

        customer = new Customer();
        customer.setFirstName(recordReader.getCellValue(headers,dataEntry,"First Name"));
        customer.setLastName(recordReader.getCellValue(headers,dataEntry,"Last Name"));
        customer.setIdNumber(recordReader.getCellValue(headers,dataEntry,"Id Number"));
        customer.setCellPhoneNumber(recordReader.getCellValue(headers,dataEntry,"Cellphone Number"));
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getCustomer() throws JsonProcessingException {
        String customerJsonString = null;
        if (customer != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            customerJsonString = objectMapper.writeValueAsString(customer);
        }
        return customerJsonString;
    }

    public Customer mapCustomer(String customerJson) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(customerJson, Customer.class);
    }
}
