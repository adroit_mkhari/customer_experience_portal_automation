package customer.experience;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@CustomerExperience"},
        features = {"src/test/java/customer/experience/features"},
        glue = {"customer/experience/steps"}
        // plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/customer_experience.html",
        //         "json:target/test-classes/custom-run-record.json"
        // }
)

public class CustomerExperienceRunner {
}
