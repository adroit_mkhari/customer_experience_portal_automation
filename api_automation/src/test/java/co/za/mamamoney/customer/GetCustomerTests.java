package co.za.mamamoney.customer;

import co.za.mamamoney.customer.model.customer.Customer;
import files.*;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GetCustomerTests extends CustomerBase {
    private Properties properties;
    private static TestCase testCase;
    private ExecutorService executorService;
    private HashSet<Callable<Scenario>> callables = new HashSet<Callable<Scenario>>();

    @BeforeClass
    public void setup() {
        try {
            FilePropertiesConfig filePropertiesConfig = new FilePropertiesConfig();
            filePropertiesConfig.loadProperties();
            properties = filePropertiesConfig.getProperties();
            String numberOfThreads = properties.getProperty("NUMBER_OF_THREADS");
            executorService = Executors.newFixedThreadPool(Integer.parseInt(numberOfThreads));

            testCase = new TestCase(
                    "Create Customer",
                    "src/main/resources/config.properties",
                    "CREATE_CUSTOMER_API_DATASHEET",
                    "GET_CUSTOMER_API_SHEET",
                    "Test Case No",
                    "Test Case Number," +
                            "Scenario Description," +
                            "Result," +
                            "Failure Reason");
            testCase.loadDataExcel();
            testCase.loadTestCaseScenarios();
            testCase.createReport("Customer_Experience_Test_Results/Get_Customer_Test_Report");
            headers = testCase.getHeaders();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    @DataProvider(name = "Excel")
    public Object[][] getTestCaseScenarios() {
        try {
            if (testCase != null) {
                Object[][] scenarioDataProvider = testCase.getScenarioDataProvider();
                return scenarioDataProvider;
            } else {
                throw new Exception("No Test Cases Loaded. ie., Test Case Is Null.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test(dataProvider = "Excel")
    public void createCustomerTest(final Scenario scenario) {
        final String run = scenario.getCellValue("Run");
        if (run.equalsIgnoreCase("Yes")) {
            callables.add(new Callable<Scenario>() {
                @Override
                public Scenario call() {
                    try {
                        // TODO: Write Test Logic Here
                        List<String> dataEntry = scenario.getDataEntry();
                        recordReader = new RecordReader(headers, "", dataEntry);
                        Customer customer = getCustomer(dataEntry);
                        String customerJson = getCustomer();

                        RestAssured.baseURI = "http://localhost:8080";
                        RequestSpecification request = RestAssured.given();

                        try {
                            String scenarioDescription = recordReader.getCellValue(
                                    headers,
                                    dataEntry,
                                    "Scenario Description");

                            if (!scenarioDescription.contains("Non-Existing")) {
                                request.contentType("application/json");
                                request.header("Authorization", "Basic YWRtaW46YWRtaW4=");
                                request.body(customerJson);
                                request.post("/api/customers");
                                Thread.sleep(10000); // Wait for kyc service
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            // DO Nothing When it fails to create. It's not the purpose of the test.
                        }

                        request = RestAssured.given();
                        request.contentType("application/json");
                        request.header("Authorization", "Basic YWRtaW46YWRtaW4=");
                        Response response = request.get(
                                "/api/customers/getCustomerByIdNumber/"
                                   + customer.getIdNumber());

                        String statusLine = response.getStatusLine();
                        String expectedGetCustomerResults = recordReader.getCellValue(
                                headers,
                                dataEntry,
                                "Expected Get Customer Results");

                        if (expectedGetCustomerResults.equalsIgnoreCase(statusLine)) {
                            if (statusLine.equalsIgnoreCase("HTTP/1.1 200 OK")) {
                                Customer targetCustomer = mapCustomer(response.getBody().prettyPrint());
                                String targetCustomerStatus = targetCustomer.getStatus();
                                String expectedStatus = recordReader.getCellValue(
                                        headers,
                                        dataEntry,
                                        "Expected Status");

                                if (!expectedStatus.equalsIgnoreCase(targetCustomerStatus)) {
                                    throw new Exception(
                                            "Wrong Expected Status: " + expectedStatus +
                                                    " -> " + targetCustomerStatus);
                                }
                            }
                        } else {
                            throw new Exception(response.getStatusCode() + " : " + statusLine);
                        }

                        scenario.setResult("PASS");
                        return scenario;
                    } catch (Exception e) {
                        scenario.setResult("FAIL");
                        scenario.setFailureReason(e.getMessage());
                        return scenario;
                    }
                }
            });
        } else {
            callables.add(new Callable<Scenario>() {
                @Override
                public Scenario call() {
                    scenario.setResult("TEST SKIPPED");
                    scenario.setFailureReason("TEST SKIPPED");
                    return scenario;
                }
            });
        }
    }

    @AfterClass
    public void finalize() {
        try {
            List<Future<Scenario>> futures = executorService.invokeAll(callables);
            for (Future<Scenario> future : futures) {
                Scenario scenario = future.get();
                String testCaseNo = scenario.getCellValue("Test Case No").split("\\.")[0];
                Integer scenarioReportRowIndex = Integer.valueOf(testCaseNo);
                testCase.setReportRowIndex(scenarioReportRowIndex);
                String scenarioDescription = scenario.getCellValue("Scenario Description");

                testCase.writeToReport(
                        Arrays.asList(testCase.getReportableFieldList()).indexOf("Test Case Number"),
                        String.valueOf(scenarioReportRowIndex),
                        TestResultReportFlag.DEFAULT);

                testCase.writeToReport(
                        Arrays.asList(testCase.getReportableFieldList()).indexOf("Scenario Description"),
                        scenarioDescription,
                        TestResultReportFlag.DEFAULT);

                testCase.writeToReport(
                        Arrays.asList(testCase.getReportableFieldList()).indexOf("Result"),
                        scenario.getResult(),
                        TestResultReportFlag.DEFAULT);

                testCase.writeToReport(
                        Arrays.asList(testCase.getReportableFieldList()).indexOf("Failure Reason"),
                        scenario.getFailureReason(),
                        TestResultReportFlag.DEFAULT);
            }
            testCase.saveReport();
            executorService.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
