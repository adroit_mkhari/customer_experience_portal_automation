package customer.experience.steps;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import customer.experience.CreateOrEditCustomer;
import customer.experience.CustomerExperiencePage;
import customer.experience.LoginPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import selenium.web.driver.DriverManagerFactory;
import selenium.web.driver.DriverType;
import selenium.web.driver.managers.DriverManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomerExperienceSteps {
    private WebDriver webDriver;
    private LoginPage loginPage;
    private CustomerExperiencePage customerExperiencePage;
    private CreateOrEditCustomer createOrEditCustomer;

    @Before
    public void setup() throws InterruptedException {
        DriverManager driverManager = DriverManagerFactory.getDriverManager(DriverType.EDGE);
        webDriver = driverManager.getWebDriver(false);
        webDriver.get("http://localhost:9000/");
        Thread.sleep(3000);
    }

    @Given("^Customer Experience Login Page$")
    public void customerExperienceLoginPage() {
        loginPage = new LoginPage(webDriver);
        Assert.assertTrue(loginPage.isDisplayed());
        Assert.assertEquals("Sign in", loginPage.getSignInPageHeader());
    }

    @Given("^User Logs In$")
    public void user_Logs_In() throws InterruptedException {
        loginPage.inputUserName("admin");
        loginPage.inputPassword("admin");
        customerExperiencePage = loginPage.clickSignIn();
        Thread.sleep(3000);
    }

    @Given("^Customer Experience Page$")
    public void customerExperiencePage() {
        Assert.assertTrue(customerExperiencePage.isDisplayed());
        Assert.assertEquals("Customers Experience Agent Portal", customerExperiencePage.getCustomerExperiencePageHeader());
    }

    @Then("^Click Create New Customer$")
    public void clickCreateNewCustomer() throws InterruptedException {
        createOrEditCustomer = customerExperiencePage.clickCreateNewCustomer();
        Thread.sleep(1000);
        Assert.assertTrue(createOrEditCustomer.isDisplayed());
        Assert.assertEquals("Create or edit a Customer", createOrEditCustomer.getPageHeader());
    }

    @Then("^Create New Customer First Name \"([^\"]*)\", Last Name \"([^\"]*)\", Cellphone Number \"([^\"]*)\", Id Number \"([^\"]*)\"$")
    public void createNewCustomer (String firstName, String lastName, String cellphoneNumber, String idNumber) throws InterruptedException {
        createOrEditCustomer.createOrEditCustomer(firstName, lastName, cellphoneNumber, idNumber);
    }

    @Then("^Search Newly Created Customer By Id Number \"([^\"]*)\" and validate \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
    public void searchNewlyCustomerByIdNumber(String idNumber, String firstName, String lastName, String cellphoneNumber, String status) throws InterruptedException {
        Thread.sleep(10000); // Wait for KYC Update
        customerExperiencePage.inputSearchText(idNumber);
        Thread.sleep(2000);
        Assert.assertEquals(firstName, customerExperiencePage.getFirstName());
        Assert.assertEquals(lastName, customerExperiencePage.getLastName());
        Assert.assertEquals(cellphoneNumber, customerExperiencePage.getCellphoneNumber());
        Assert.assertEquals(idNumber, customerExperiencePage.getIdNumber());
        Assert.assertEquals(status, customerExperiencePage.getStatus());
    }

    @Then("^Search Customer By Id Number \"([^\"]*)\"$")
    public void searchCustomerByIdNumber(String idNumber) throws InterruptedException {
        customerExperiencePage.inputSearchText(idNumber);
        Thread.sleep(2000);
        customerExperiencePage.clickEdit();
    }

    @Then("^Update Customer First Name \"([^\"]*)\", Last Name \"([^\"]*)\", Cellphone Number \"([^\"]*)\"$")
    public void updateCustomer(String firstName, String lastName, String cellphoneNumber) throws Throwable {
        createOrEditCustomer = createOrEditCustomer != null ? createOrEditCustomer : new CreateOrEditCustomer(webDriver);
        createOrEditCustomer.createOrEditCustomer(firstName, lastName, cellphoneNumber, "");
    }

    @After
    public void finalize() {
        try {
            if (webDriver != null) {
                webDriver.quit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
