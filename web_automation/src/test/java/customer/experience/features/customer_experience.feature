@CustomerExperience
Feature: Customer Experience
  Background:
    Given Customer Experience Login Page
    And User Logs In

  Scenario Outline: Create Customer
    Given Customer Experience Page
      Then Click Create New Customer
      Then Create New Customer First Name "<name>", Last Name "<lastName>", Cellphone Number "<cellphoneNumber>", Id Number "<idNumber>"
      Then Search Newly Created Customer By Id Number "<idNumber>" and validate "<name>", "<lastName>", "<cellphoneNumber>" and "<expectedStatus>"

    Examples:
      | name      | lastName | cellphoneNumber | idNumber      | expectedStatus |
      | Adroit    | Mkhari   | 0714985774      | 9101115911086 | ACTIVE         |
      | Vukosi    | Ngobeni  | 0724985774      | 9201115911086 | BLOCKED        |
      | Rivonigo  | Mkhari   | 0734985774      | 9301115911086 | BLOCKED        |
      | Ripfumelo | Mkhari   | 0684985774      | 9401115911086 | BLOCKED        |
      | Nseketelo | Mkhari   | 0684985074      | 9501115911086 | BLOCKED        |
      | Nseketelo | Mkhari   | 0684985074      | 950111591     |                |
      | Nseketelo | Mkhari   | 0684985         | 9501115911086 |                |

  Scenario Outline: Create Customer
    Given Customer Experience Page
    Then Search Customer By Id Number "<idNumber>"
    Then Update Customer First Name "<name>", Last Name "<lastName>", Cellphone Number "<cellphoneNumber>"

    Examples:
      | idNumber      | name            | lastName       | cellphoneNumber |
      | 9101115911086 | Update Adroit   | Mkhari         | 0714985774      |
      | 9201115911086 | Vukosi          | Update Ngobeni | 0724985774      |
      | 9301115911086 | Update Rivonigo | Update Mkhari  | 0734985774      |
      | 9401115911086 | Ripfumelo       | Mkhari         | 0684985         |
