package files;

public enum TestResultReportFlag {
    SUCCESS,
    WARNING,
    FAIL,
    DEFAULT,
    PICTURE
}
