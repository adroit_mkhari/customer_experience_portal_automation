package selenium.web.driver.managers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

import java.io.File;

public class EdgeDriverManager extends DriverManager {
    @Override
    protected void createDriver() {
        EdgeOptions options = new EdgeOptions();
        if (!driverBinary.isEmpty()) {
            System.setProperty(
            "webdriver.edge.driver",
            System.getProperty("user.dir") +
            File.separator + "src" +
            File.separator + "main" +
            File.separator + "resources" +
            File.separator + driverBinary);
        } else {
            WebDriverManager.edgedriver().setup();
        }
        driver = new EdgeDriver(options);
    }

    @Override
    protected void createDriver(boolean headless) {
        if (headless) {
            // TODO
        } else {
            createDriver();
        }
    }
}
